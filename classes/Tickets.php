<?php

require_once 'crud.php';

class Tickets extends crud{

	protected $table = 'tickets';
	private $requisitante;
	private $tpequipameto;
	private $problemas;
	private $descricao;
	private $prioridade;
	private $data;
	private $opcao;
	private $obs;

	public function setRequisitante($requisitante){
		$this->requisitante = $requisitante;
	}

	public function settpEquipamento($tpequipamento){
		$this->tpequipamento = $tpequipamento;
	}

	// public function setProblemas($problemas){
	// 	$this->problemas = $problemas;
	// }

	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}


	public function setPrioridade($prioridade){
		$this->prioridade = $prioridade;
	}

	public function setDate($date){
		$this->date = $date;
	}

	public function setOpcao($opcao){
		$this->opcao = $opcao;
	}

	public function setObs($obs){
		$this->obs = $obs;
	}

	public function setProblemas($tpProblema, $descProblema, $requisitante) {

		$today = date('Y-m-d');

			$sql = "INSERT INTO tickets_problemas
				(tpProblema, descProblema, created_at, userid)
				VALUES
				(:tpProblema, :descProblema, :created_at, :userid)";
			$stmt = DB::prepare($sql);
			$stmt->bindParam(':tpProblema', $tpProblema);
			$stmt->bindParam(':descProblema', $descProblema);
			$stmt->bindParam(':userid', $requisitante);
			$stmt->bindParam(':created_at', $today);
			return $stmt->execute();

	}



	public function insert(){

		$sql  = "INSERT INTO $this->table (requisitante, tpequipamento, problemas, descricao, prioridade, date, opcao, obs) VALUES(:requisitante, :tpequipamento, :problemas, :descricao, :prioridade, :date, :opcao, :obs)";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':requisitante', $this->requisitante);
		$stmt->bindParam(':tpequipamento', $this->tpequipamento);
		$stmt->bindParam(':problemas', $this->problemas);
		$stmt->bindParam(':descricao', $this->descricao);
		$stmt->bindParam(':prioridade', $this->prioridade);
		$stmt->bindParam(':date', $this->date);
		$stmt->bindParam(':opcao', $this->opcao);
		$stmt->bindParam(':obs', $this->obs);
		return $stmt->execute();

	}

	public function update($id){

		$sql  = "UPDATE $this->table SET requisitante = :requisitante, tpequipamento = :tpequipamento, problemas = :problemas, descricao = :descricao, prioridade = :prioridade, date = :date, opcao = :opcao, obs = :obs  WHERE id = :id";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':requisitante', $this->requisitante);
		$stmt->bindParam(':tpequipamento', $this->tpequipamento);
		$stmt->bindParam(':problemas', $this->problemas);
		$stmt->bindParam(':descricao', $this->descricao);
		$stmt->bindParam(':prioridade', $this->prioridade);
		$stmt->bindParam(':date', $this->date);
		$stmt->bindParam(':opcao', $this->opcao);
		$stmt->bindParam(':obs', $this->obs);
		$stmt->bindParam(':id', $id);
		return $stmt->execute();

	}

}