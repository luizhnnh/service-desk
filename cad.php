<!DOCTYPE html>
<html lang="en">
<head>
	<title>Service Desk - Cadastro</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================--><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
	   </script>
	<script>
		   function validarSenha(){
			senha1 = document.f1.senha1.value
			senha2 = document.f1.senha2.value
 
			if (senha1 == senha2)
			alert("SENHAS IGUAIS")
				else
				alert("SENHAS DIFERENTES")
			}
	 </script>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form p-l-55 p-r-55 p-t-178">
					<span class="login100-form-title">
						Service Desk - LH Info Web
					</span>

					<div class="wrap-input100 validate-input m-b-16" required="required" data-validate="Por favor insira seu nome de usuario" >
						<input class="input100" type="text" name="username" placeholder="*Nome de usuario">
						<span class="focus-input100"></span>
					</div>
					
					<div class="wrap-input100 validate-input m-b-16" data-validate="Por favor insira seu e-mail">
						<input class="input100" type="email" name="email" placeholder="*E-mail">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Por favor insira uma senha">
						<input class="input100" type="password" name="senha1" placeholder="*Senha">
						<span class="focus-input100"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" data-validate = "Por favor confirme sua senha">
						<input class="input100" type="password" name="senha2" placeholder="*Confirme sua senha">
						<span class="focus-input100"></span>
					</div>

					<div class="text-right p-t-13 p-b-23">
						<span class="txt1">
							*Preenchimento Obrigatorio
						</span>

						
					</div>

					
					<!--<input type="button" class="btn btn-warning" value="Validar Senha" onClick="validarSenha()">-->
					
					<div class="container-login100-form-btn">
						
						<button class="login100-form-btn-cad input"> Cadastrar</button>
							
					</div>

					
				</form>
				<div class="container-login100-form-btn" >
					
						 <a href="index.html">
						<button class="login100-form-btn ">
							VOLTAR AO MENU
						</button>
						</a>
						
						
					</div>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>