<?php
	// function __autoload(Tickets){
	// 	require_once 'classes/' . $class_name . '.php';
	// }
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



	require_once('classes/'.'Tickets.php');
	include('classes/'.'usuarios.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Service Desk - Cadastro</title>	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================--><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
	   </script>
	
<?
$data=date("d/m/Y");
?>
</head>
<body>
	
	
	<div class="limiter">
		<div class="container-login100">

		<?php
	
		$ticket = new Tickets();

		if(isset($_POST['enviar'])):

			// $requisitante  = $_POST['requisitante'];
			// $tpequipamento = $_POST['tpequipamento'];
			// // $problemas = $_POST['Tpproblemas'];
			// $descricao = $_POST['descricao'];
			// $prioridade = $_POST['prioridade'];
			// $date = $_POST['date'];
			// $opcao = $_POST['opcao'];
			// $obs = $_POST['obs'];

			// $ticket->setRequisitante($requisitante);
			// $ticket->settpEquipamento($tpequipamento);
			// // $ticket->setProblemas($problemas);
			// $ticket->setDescricao($descricao);
			// $ticket->setPrioridade($prioridade);
			// $ticket->setDate($date);
			// $ticket->setOpcao($opcao);
			// $ticket->setObs($obs);
			$amount = count($_POST['tpProblema']);
			$i = 0;
			while($i < $amount)
			{
				$ticket->setProblemas($_POST['tpProblema'][$i], $_POST['descProblema'][$i], $_POST['requisitante']);
				$i++;
			}


			# Insert
			if($ticket->insert()){
				echo "Inserido com sucesso!";
			}

		endif;

		?>
			<div class="wrap-login100">
				<form class="login100-form validate-form p-l-55 p-r-55 p-t-178" method="post">
					<span class="login100-form-title">
						Service Desk - LH Info Web
					</span>
					<form id="form1" role="form" method="post" action="Tickets.php">
	<fieldset>
		
		<div class="row">
			<div class="form-group col-lg-12" >
				<label for="inputuser">*Qual o responsavel pelo chamado?</label>
				<select class="form-control" id="requisitante" name="requisitante" required>
					<option value="-1">Escolha uma opção</option>
					<?php

						$user = new usuarios();
						$users = $user->findAll();
						/** Busca no banco de dados os usuarios e cria as opções! 
						 *  Detalhe: Conexões PDO retornam Objetos portanto para acessar uma "Key" não se usa [''] e sim ->
						*/
						foreach ($users as $value) {
							echo '<option value="'.$value->id.'">'.$value->name.'</option>';
						}
					?>
				</select>
			</div>
			<br>
			<div class="form-group col-lg-12">
				<label for="inputequipa">*Qual o tipo de equipamento apresenta falhas?</label>
				<select class="form-control" name="tpequipamento" required>
					<option value="-1">Escolha uma opção</option>
					<option value="1">MATFIN01</option>
					<option value="2">MATFIN02</option>
					<option value="3">MATFIN03</option>
					<option value="4">MATDEPTOPESSOAL01</option>
					<option value="5">MATDEPTOPESSOAL02</option>
					<option value="6">MATDEPTOPESSOAL03</option>
					<option value="7">MATCONTRECEB01</option>
					<option value="8">MATCONTRECEB02</option>
					<option value="9">MATCONTRECEB03</option>
				</select>
			</div>
			<br>
		
		<div class="container">
      
      <form  id="form2" method="post" >
        
        <div class="form-group">
          <label for="exampleFormControlSelect1">*Quais os problemas encontrados?</label>
          <select class="form-control" name="problemas" id="combo">
          <option value="-1">Escolha uma opção</option>
    			<option value="Computador não liga">Computador não liga</option>
   				<option value="Computador reiniciando">Computador reiniciando</option>
    			<option value="Computador travando">Computador travando</option>
    			<option value="Computador fazendo barulho">Computador fazendo barulho</option>
    			<option value="Computador apitando">Computador apitando</option>
    			<option value="Computador travando">Computador travando</option>
    			<option value="Computador travando">Computador travando</option>
    			<option value="Monitor não liga">Monitor não liga</option>
    			<option value="Mouse/Teclado não funcionam">Mouse/Teclado não funcionam</option>
          </select>
        </div>
        <div class="form-group">
          <label for="exampleFormControlInput1">Caso queira, faça uma breve descrição sobre o problema do equipamento</label>
			<textarea class="form-control" rows="3" name="descricao" id="desc"></textarea>
        </div>
        <button type="button" class="btn btn-primary" onclick="return insertValues('myTable')">Inserir</button>
      </form>
      <br />
      <br />
      <table class="table">
       <thead>
       <tr>
           <th>Problema</th>
           <th>Descrição</th>
       </tr>
   </thead>
        <tbody id="myTable"></tbody>
      </table>
    </div>				
			<div class="form-group col-lg-12">
				<label for="inputprio">*Prioridade</label>
				<select class="form-control" name="prioridade" required>
					<option value="-1">Escolha uma opção</option>
					<option value="1">Alta</option>
					<option value="2">Normal</option>
					<option value="3">Baixa</option>
					
					
				</select>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group col-lg-6">
				<label for="inputdateoco">Data da ocorrência</label>
				<input type="date" name="date"  class="form-control"value="<?php echo date("Y-m-d");?>" enable>
				</div>
		</div>
		<br>
		
		<div class="radio" >
			<label for="inputv">Escolha uma opção:</label>
			<br>
			<input type="radio" name="opOcorrencia" id="opcao" value="Esta é a primeira ocorrência" checked="">
			<label for="optionsRadios1">Esta é a primeira ocorrência</label>
		</div>
		<div class="radio">
			<input type="radio" name="opOcorrencia" id="opcao" value="Problema recorrente">
			<label for="optionsRadios2">Problema recorrente</label>
		</div>
		<br>
		<label for="descrisao">Observação</label>
		<textarea class="form-control" name="obs" rows="3"  id="tt" ></textarea>
		<br>
		<div class="text-right p-t-13 p-b-23" >
						<span class="txt1">
							*Preenchimento Obrigatorio
						</span>

						
					</div>
					<hr><br>
					
					
		<div class="box-actions">
			<input type="submit" name="enviar" class="btn btn-primary" onclick=""></button>
			<button class="btn btn-danger">Cancelar</button>
			<br><br>
		
		</div>
		
 
	</fieldset>
</form>
<br><br>
				</form>
				<div class="container-login100-form-btn" >
					
						 <a href="index.php">
						<button class="login100-form-btn ">
							VOLTAR AO MENU
						</button>
						</a>
						
						
					</div>
			</div>
		</div>
	</div>

		<script>
		document.getElementById('DateField').valueAsDate = new Date();
		</script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
      src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
      integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
      integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
      crossorigin="anonymous"
    ></script>

    <script>
	  var comboValue = document.getElementById("combo");
      var textValue = document.getElementById("desc");
      

      function insertValues(idTabela) {
        var newRow = document.createElement("tr");
				/**
						Foi modificado para poder inserir um input com o valor que será passado para o backend
						o nome do input é um @array[] para poder ser inserido mais de um elemento.
				 */
        newRow.insertCell(0).innerHTML = '<input type="hidden" value="'+combo.value+'" name="tpProblema[]">'+combo.value;
				newRow.insertCell(1).innerHTML = '<input type="hidden" value="'+textValue.value+'" name="descProblema[]">'+textValue.value;
        document.getElementById(idTabela).appendChild(newRow);
        return false;
      }
    </script>
				
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
